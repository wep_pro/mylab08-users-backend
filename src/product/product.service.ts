import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { product } from './entities/product.entity';
let products: product[] = [
  { id: 1, name: 'Product1', price: 10 },
  { id: 2, name: 'Product2', price: 100 },
  { id: 3, name: 'Product3', price: 1000 },
];

let lastProductId = 4;
@Injectable()
export class ProductService {
  create(createUserDto: CreateProductDto) {
    const newProduct: product = {
      id: lastProductId++,
      ...createUserDto,
    };
    products.push(newProduct);
    return newProduct;
  }

  findAll() {
    return products;
  }

  findOne(id: number) {
    const index = products.findIndex((product) => {
      return product.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    return products[index];
  }

  update(id: number, updateUserDto: UpdateProductDto) {
    const index = products.findIndex((user) => {
      return user.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    const updateUser: product = {
      ...products[index],
      ...updateUserDto,
    };
    products[index] = updateUser;
    return updateUser;
  }

  remove(id: number) {
    const index = products.findIndex((user) => {
      return user.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    const deletedUser = products[index];
    products.splice(index, 1);
    return deletedUser;
  }

  reset() {
    products = [
      { id: 1, name: 'Product1', price: 10 },
      { id: 2, name: 'Product2', price: 100 },
      { id: 3, name: 'Product3', price: 1000 },
    ];
    lastProductId = 4;
    return 'RESET';
  }
}
